﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(TileSpawn))]
public class TileSpawnEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        TileSpawn tile = target as TileSpawn;

        if (GUILayout.Button("Select Neighbours"))
        {
            TileSpawn[] tiles = tile.GetNeighbours();
            GameObject[] objects = new GameObject[6];

            int j = 0;
            for (int i = 0; i < tiles.Length; i++)
                if (tiles[i] != null)
                    objects[j++] = tiles[i].gameObject;

            Selection.objects = objects;
        }

        if (tile.isBorder)
        {
            if (GUILayout.Button("Spawn Tile"))
            {
                tile.Spawn();
            }
        }
        else
        {
            if (GUILayout.Button("Despawn Tile"))
            {
                tile.Despawn();
            }
        }
    }

}