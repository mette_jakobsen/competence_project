﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CameraMovePoint))]
public class CameraMovementEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        CameraMovePoint point = target as CameraMovePoint;

        if (GUILayout.Button("Move Here"))
        {
            GameManager.instance.camera.MoveTo(point);
        }

    }
}
