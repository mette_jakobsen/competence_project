﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour {

    private const float DESTINATION_UPDATE_INTERVAL = 0.5f;

    private NavMeshAgent agent;
    private float timestamp;
    private TileSpawn _target;

	void Start () {
        agent = GetComponent<NavMeshAgent>();
	}
	
	void Update () {
        if (Time.time >= timestamp && target)
        {
            agent.destination = target.transform.position;
            updateTimestamp();
        }
    }

    public TileSpawn target
    {
        get
        {
            return _target;
        }
    }

    public void MoveTowards(TileSpawn tile)
    {
        agent.destination = tile.transform.position;

        if (_target && _target.isBorder)
            _target.obstacle.enabled = true;

        if (tile.isBorder && GameManager.instance.resource > 0)
            tile.obstacle.enabled = false;

        _target = tile;
        updateTimestamp();
    }

    private void updateTimestamp()
    {
        timestamp = Time.time + DESTINATION_UPDATE_INTERVAL;
    }
}
