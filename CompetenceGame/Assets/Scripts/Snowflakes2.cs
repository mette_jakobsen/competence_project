﻿using UnityEngine;
using System.Collections;

public class Snowflakes2 : MonoBehaviour
{
    private float radiusOfFirstRing;
    private float radiusOfSecondRing;
    private float thickness;
    private float lengthOfSideBranchOne;
    private float lengthOfSideBranchTwo;
    private float lengthOfSideBranchThree;

    private float radiusOfThirdRing;
    private Vector3[] verts;
    private Vector3[] flippedVerts;
    private Vector3[] secondVerts;
    private Vector3[] flippedSecondVerts;
    private Vector3[] allVerts;
    private int[] pattern;
    private Mesh mesh;
    private int numberOfTriangles = 3;
    private float mirrorOffset;
    //
    private float tf, s, s1, s2, s3, s4, a1, a2, a3, a4, a5, a6, a7, a8, angle, angleOffset, extraOffset;
    private Vector3 thisStartPos, thisEndPos, thisMidPos, thisFlipPos, startPos, midPos, endPos, flippedPos;

    void Start()
    {
        gameObject.AddComponent<MeshFilter>();
        mesh = GetComponent<MeshFilter>().mesh;
        mesh.Clear();

        //~~~~~~~~~~~~~~
        
        setRandomValues();
        mirrorOffset = (2 * Mathf.PI) / 6;
        angleOffset = (2 * Mathf.PI) / (12 + (6 * (int)thickness));
        extraOffset = (2 * Mathf.PI) / 6;
        //~~~~~~~~~~~~~~
     
        //Set sizes of all the array because C# is a douche which doesn't let me append...
        verts = new Vector3[3 * numberOfTriangles * 6];
        flippedVerts = new Vector3[3 * numberOfTriangles * 6];
        secondVerts = new Vector3[18*3];
        flippedSecondVerts = new Vector3[18 * 3];
        allVerts = new Vector3[verts.Length + secondVerts.Length+ flippedVerts.Length + flippedSecondVerts.Length];
        pattern = new int[verts.Length + secondVerts.Length + flippedVerts.Length + flippedSecondVerts.Length];
        for (int i = 0; i < pattern.Length; i++)
            pattern[i] = i;

        //Call all the triangles six times, one for each mirror.
        for (int j = 0; j < 6; j++)
        {
            for (int i = 0; i < numberOfTriangles; i++)
            {
                drawTriangle(i, mirrorOffset * (j + 1), j);
            }
        }
        //Combine all the arrays after the above function has filled them out.
        combineArrays();

        //Actually draw the mesh
        mesh.vertices = allVerts;
        mesh.triangles = pattern;

    }

    void drawTriangle(int numTri, float angle, int numMirror)
    {
        //Use the Random parameters.
        if (numTri == 0)
        {
            s1 = 0;
            s2 = radiusOfFirstRing;
            s3 = radiusOfFirstRing / 2;
        }
        else if (numTri == 1)
        {
            s1 = radiusOfFirstRing;
            s2 = radiusOfFirstRing + radiusOfSecondRing;
            s3 = radiusOfFirstRing + (radiusOfSecondRing/ 2);
        }
        else if (numTri == 2)
        {
            s1 = radiusOfFirstRing + radiusOfSecondRing;
            s2 = radiusOfFirstRing + radiusOfSecondRing + radiusOfThirdRing;
            s3 = radiusOfFirstRing + radiusOfSecondRing + (radiusOfThirdRing / 2);            
        }
        
        //Actually do the math to draw the triangles
        a1 = Mathf.Cos(angle);
        a2 = Mathf.Sin(angle);
        a3 = Mathf.Cos(angle + angleOffset);
        a4 = Mathf.Sin(angle + angleOffset);
        a5 = Mathf.Cos(angle - angleOffset);
        a6 = Mathf.Sin(angle - angleOffset);

        //Added because sin(0) = 0
        if (a2 == 0)
            a2 = 1;
        if (a4 == 0)
            a4 = 1;
        if (a6 == 0)
            a4 = 1;
        //Compute the position of the vertices 
        thisStartPos = new Vector3(a1 * s1, 0, a2 * s1);
        thisEndPos = new Vector3(a1 * s2, 0, a2 * s2);
        thisMidPos = new Vector3(a3 * s3, 0, a4 * s3);
        thisFlipPos = new Vector3(a5 * s3, 0, a6* s3);
        
        //assign the positions to their arrays
        verts[0 + (numTri * numberOfTriangles) + (numMirror * (numberOfTriangles * numberOfTriangles))] = thisStartPos;
        verts[1 + (numTri * numberOfTriangles) + (numMirror * (numberOfTriangles * numberOfTriangles))] = thisMidPos;
        verts[2 + (numTri * numberOfTriangles) + (numMirror * (numberOfTriangles * numberOfTriangles))] = thisEndPos;

        flippedVerts[0 + (numTri * numberOfTriangles) + (numMirror * (numberOfTriangles * numberOfTriangles))] = thisStartPos;
        flippedVerts[1 + (numTri * numberOfTriangles) + (numMirror * (numberOfTriangles * numberOfTriangles))] = thisEndPos;
        flippedVerts[2 + (numTri * numberOfTriangles) + (numMirror * (numberOfTriangles * numberOfTriangles))] = thisFlipPos;

        drawSecondTriangle(thisStartPos, numTri, (numTri * numberOfTriangles) + (numMirror * (numberOfTriangles * numberOfTriangles)), angle);

    }
    void drawSecondTriangle(Vector3 startPoint, int numTri, int numTriangle, float currentAngle)
    {
        if (numTri == 0)
        {
            s = lengthOfSideBranchOne;
        }
        else if (numTri == 1)
        {
            s = lengthOfSideBranchTwo;
        }
        else if (numTri == 2)
        {
            s = lengthOfSideBranchThree;
        }

        angle = currentAngle;

        a1 = Mathf.Cos(angle + extraOffset);
        a2 = Mathf.Sin(angle + extraOffset);
        a3 = Mathf.Cos(angle + angleOffset + extraOffset);
        a4 = Mathf.Sin(angle + angleOffset + extraOffset);
        a5 = Mathf.Cos(angle - (angleOffset + extraOffset));
        a6 = Mathf.Sin(angle - (angleOffset + extraOffset));
        a7 = Mathf.Cos(angle - mirrorOffset);
        a8 = Mathf.Sin(angle - mirrorOffset);


        //Added because sin(0) = 0
        if (a2 == 0)
            a2 = 1;
        if (a4 == 0)
            a4 = 1;
        if (a6 == 0)
            a4 = 1;
        if (a8 == 0)
            a4 = 1;

        startPos = startPoint;
        endPos = new Vector3(startPos.x+ a1 * s, 0, startPos.z + a2 * s);
        midPos = new Vector3(startPos.x + a3 * (s/2), 0, startPos.z + a4 * (s / 2));
        flippedPos = new Vector3(startPos.x + a5 * (s / 2), 0, startPos.z + a6 * (s / 2));
        Vector3 flippedEndPos = new Vector3(startPos.x + a7 * s, 0, startPos.z + a8 * s);

        secondVerts[0 + numTriangle] = startPos;
        secondVerts[1 + numTriangle] = midPos;
        secondVerts[2 + numTriangle] = endPos;

        flippedSecondVerts[0 + numTriangle] = startPos;
        flippedSecondVerts[1 + numTriangle] = flippedEndPos;
        flippedSecondVerts[2 + numTriangle] = flippedPos;

    }

    void combineArrays()
    {
        for (int i = 0; i < verts.Length; i++)
        {
            allVerts[i] = verts[i];
        }
        for (int i = 0; i < secondVerts.Length; i++)
        {
            allVerts[i + verts.Length] = secondVerts[i];
        }
        for (int i = 0; i < flippedVerts.Length; i++)
        {
            allVerts[i + verts.Length+ secondVerts.Length] = flippedVerts[i];
        }

        for (int i = 0; i < flippedSecondVerts.Length; i++)
        {
            allVerts[i + verts.Length + secondVerts.Length + flippedVerts.Length] = flippedSecondVerts[i];
        }
    }

    void setRandomValues()
    {
        radiusOfFirstRing = Random.Range(0.0f,0.5f);
        radiusOfSecondRing = Random.Range(0.2f,0.5f);
        radiusOfThirdRing = 1.0f - radiusOfFirstRing - radiusOfSecondRing;
        thickness = Random.Range(1.0f,3.9f);
        lengthOfSideBranchOne = Random.Range(0.1f, 1.0f);
        lengthOfSideBranchTwo = Random.Range(0.2f, 0.5f);        
        lengthOfSideBranchThree = Random.Range(0.0f, radiusOfThirdRing);
    }
}