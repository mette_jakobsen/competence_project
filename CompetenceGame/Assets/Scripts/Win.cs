﻿using UnityEngine;
using System.Collections;

public class Win : MonoBehaviour {

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject == GameManager.instance.player.gameObject)
        {
            GameManager.instance.GameWon();
        }
    }
}
