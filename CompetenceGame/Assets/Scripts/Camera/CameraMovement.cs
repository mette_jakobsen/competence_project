﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
    public AnimationCurve curve;
    public float moveTime;
    public CameraMovePoint startPosition;
    public float dangerRadius;

    private CameraMovePoint current;
    private Transform player;
    private Vector3 direction, _position, _prevPosition;
    private float distance, timestamp, _deltaTime, _normTime, _distanceToPlayer, _newDistanceToPlayer;
    private Particle[] objects;
    private Particle obj;
    private int i;

    private Quaternion rotation;

    void Start()
    {
        current = startPosition;
        player = GameManager.instance.player.transform;
        _position = current.transform.position;
        timestamp = 0;
    }

    void Update()
    {
        _deltaTime = Time.time - timestamp;
        _normTime = _deltaTime / moveTime;
        transform.position = _normTime < 1 ? _position + direction * distance * curve.Evaluate(1 - _normTime) : _position;

        objects = GameObject.FindObjectsOfType<Particle>();
        obj = null;

        for (i = 0; i < objects.Length; i++)
        {
            _newDistanceToPlayer = Vector3.Distance(GameManager.instance.player.transform.position, objects[i].transform.position);
            if (obj == null || _newDistanceToPlayer < _distanceToPlayer)
            {
                _distanceToPlayer = _newDistanceToPlayer;
                obj = objects[i];
            }
        }

        transform.LookAt(obj != null && _distanceToPlayer < dangerRadius ? obj.transform : GameManager.instance.finish);
        rotation = transform.rotation;

        transform.LookAt(player);
        transform.rotation = Quaternion.Lerp(rotation, transform.rotation, 0.5f);
    }

    public void MoveTo(CameraMovePoint point)
    {
        if (point != current)
        {
            _prevPosition = _position;
            _position = point.transform.position;
            direction = (_prevPosition - _position).normalized;
            distance = Vector3.Distance(_position, _prevPosition);
            timestamp = Time.time;
            current = point;
        }
    }
}