﻿using UnityEngine;
using System.Collections;

public class TileSpawn : MonoBehaviour {

    private const float SPAWN_INTERVAL = 5.0f;
    private const float SPAWN_CHANCE = 0.16f;

    public bool isBorder;
    public Pickup pickup;

    private int indexX, indexZ;
    private int[,] kernel = new int[6,2];

    private float timestamp;

    [HideInInspector]
    public NavMeshObstacle obstacle;

    void Start () {
        obstacle = GetComponent<NavMeshObstacle>();
        if (isBorder)
            GetComponent<MeshRenderer>().enabled = GameManager.instance.resource > 0;
        timestamp = SPAWN_INTERVAL * Random.value;
    }
	
	void Update () {
        if (!isBorder && pickup == null && Time.time >= timestamp)
        {
            if (Random.value < SPAWN_CHANCE + GameManager.instance.difficulty* 0.04f)
            {
                pickup = Instantiate(GameManager.instance.spawner.pickup).GetComponent<Pickup>();
                pickup.transform.position = new Vector3(transform.position.x, 1.0f, transform.position.z);
                pickup.tile = this;
            }
            timestamp = Time.time + SPAWN_INTERVAL;
        }
    }

    void OnMouseDown()
    {
        GameManager.instance.player.MoveTowards(this);
    }

    void OnTriggerEnter(Collider collider)
    {
        Particle particle = collider.gameObject.GetComponent<Particle>();
        if (particle && !isBorder)
        {
            Despawn();
            Destroy(particle.gameObject);
        }

        if (collider.gameObject == GameManager.instance.player.gameObject && !isBorder)
            GameManager.instance.playerTile = this;

        if (this == GameManager.instance.player.target && collider.gameObject == GameManager.instance.player.gameObject && GameManager.instance.resource > 0 &&
            Spawn()
        )
            GameManager.instance.removeResource();
    }

    public void pickedUp()
    {
        Destroy(pickup.gameObject);
        pickup = null;
        GameManager.instance.addResource();
    }

    public void updateKernel(int indexX, int indexZ)
    {
        this.indexX = indexX;
        this.indexZ = indexZ;
        int i = (Mathf.Abs(indexZ) % 2 > 0 ? 1 : -1);

        kernel[0, 0] = indexX;
        kernel[0, 1] = indexZ - 1;

        kernel[1, 0] = indexX;
        kernel[1, 1] = indexZ + 1;

        kernel[2, 0] = indexX - 1;
        kernel[2, 1] = indexZ;

        kernel[3, 0] = indexX + 1;
        kernel[3, 1] = indexZ;

        kernel[4, 0] = indexX + i;
        kernel[4, 1] = indexZ - 1;

        kernel[5, 0] = indexX + i;
        kernel[5, 1] = indexZ + 1;
    }

    public bool Spawn()
    {
        if (isBorder)
        {
            GameManager.instance.spawner.AddTile(indexX, indexZ);
            return true;
        }
        return false;
    }

    public void Despawn()
    {
        if (!isBorder)
        {
            TileSpawn[] neighbours = GetNeighbours();
            for (int i = 0; i < neighbours.Length; i++)
                if (neighbours[i] && neighbours[i].isBorder && !neighbours[i].HasAnchorTile(this))
                    Destroy(neighbours[i].gameObject);

            if (GameManager.instance.playerTile == this)
                GameManager.instance.GameLost();

            if (HasAnchorTile(this))
                GameManager.instance.spawner.AddTile(indexX, indexZ, true, true);
            else
                Destroy(gameObject);

            if (pickup != null)
                Destroy(pickup.gameObject);
        }
    }

    public void SpawnBorders()
    {
        TileSpawn[] neighbours = GetNeighbours();

        for (int i = 0; i < neighbours.Length; i++)
            if (neighbours[i] == null)
                GameManager.instance.spawner.AddTile(kernel[i, 0], kernel[i, 1], true);
    }

    public bool HasAnchorTile(TileSpawn removedTile = null)
    {
        for (int i = 0; i < 6; i++)
        {
            TileSpawn tile = GameManager.instance.spawner.GetTileAt(kernel[i, 0], kernel[i, 1]);
            if (tile != null && tile != removedTile && !tile.isBorder)
                return true;
        }
        return false;
    }

    public TileSpawn[] GetNeighbours()
    {
        TileSpawn[] neighbours = new TileSpawn[6];

        for (int i = 0; i < 6; i++)
            neighbours[i] = GameManager.instance.spawner.GetTileAt(kernel[i, 0], kernel[i, 1]);

        return neighbours;
    }
}
