﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileSpawner : MonoBehaviour {

    public TileSpawn tile;
    public TileSpawn tileBorder;
    public GameObject pickup;

    private List<GameObject> list;
    private int offsetX, offsetZ;

    void Start () {
        list = new List<GameObject>();
        offsetX = 0;
        offsetZ = 0;
        GameManager.instance.player.transform.position = AddTile(0, 0).transform.position;
        AddTile(0, 1);
        AddTile(0, -1);
        AddTile(-1, -1);
        AddTile(-1, 1);
        AddTile(1, 0);
        AddTile(-1, 0);
    }

    public TileSpawn AddTile(int indexX, int indexZ, bool isBorder = false, bool force = false)
    {
        GameObject obj;
        TileSpawn prevTile;
        TileSpawn newTile = null;
        int i;

        while (indexZ < - offsetZ)
            createParent("TileParent" + (- ++offsetZ), - offsetZ, 0);

        while ((obj = (i = list.Count) > offsetZ + indexZ ? list[offsetZ + indexZ] : null) == null)
            obj = createParent("TileParent" + (i - offsetZ), (i - offsetZ), list.Count);

        prevTile = GetTileAt(indexX, indexZ);
        if (force || prevTile == null || prevTile.isBorder)
        {
            if (prevTile != null)
            {
                Destroy(prevTile.gameObject);
            }
            newTile = Instantiate(isBorder ? tileBorder : tile, obj.transform) as TileSpawn;
            newTile.transform.position = new Vector3((float)(indexX - offsetX) * TileDraw.WIDTH * tile.transform.localScale.x + obj.transform.position.x, 0, obj.transform.position.z);
            newTile.updateKernel(indexX, indexZ);

            if (!isBorder)
            {
                newTile.SpawnBorders();
            }
        }
        return newTile;
    }

    private GameObject createParent(string name, int indexZ, int listIndex)
    {
        GameObject obj = new GameObject(name);
        list.Insert(listIndex,obj);
        obj.transform.parent = transform;
        obj.transform.position = new Vector3(Mathf.Abs(indexZ) % 2 > 0 ? TileDraw.WIDTH * tile.transform.localScale.x * 0.5f : 0, 0, indexZ * tile.transform.localScale.z * 0.75f);
        return obj;
    }

    public TileSpawn GetTileAt(int indexX, int indexZ)
    {
        if (indexZ < -offsetZ || list.Count <= offsetZ + indexZ)
            return null;

        GameObject obj = list[offsetZ + indexZ];
        TileSpawn[] children = obj.GetComponentsInChildren<TileSpawn>();

        for (int i = 0; i < children.Length; i++)
            if (children[i].transform.position.x == (float)(indexX - offsetX) * TileDraw.WIDTH * tile.transform.localScale.x + obj.transform.position.x)
                return children[i];
        return null;
    }
}
