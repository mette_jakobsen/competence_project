﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshCollider))]
[RequireComponent(typeof(MeshFilter))]
public class TileDraw : MonoBehaviour {

    private const float SCALE = 0.5f;
    public const float WIDTH = 0.866025f;

    private float thickness = 1.0f;
    private Mesh mesh;
    private Vector3[] verts;
    private int[] drawingPattern;

    void Start() {
        mesh = GetComponent<MeshFilter>().mesh;

        verts = new Vector3[14];
        drawingPattern = new int[72];

        computeHexagon();
        drawHexagon();

        mesh.Clear();
        mesh.vertices = verts;
        mesh.triangles = drawingPattern;
        GetComponent<MeshCollider>().sharedMesh = mesh;
        enabled = false;
    }

    void Update() {

    }

    void computeHexagon() {
        float sqrtOfThree = Mathf.Sqrt(3);

        for (int i = 0; i < 2; i++) {
            verts[(i * 7) + 0] = new Vector3(0, 0 - i * thickness, 0) * SCALE;
            verts[(i * 7) + 1] = new Vector3(0, 0 - i * thickness, 1) * SCALE;
            verts[(i * 7) + 2] = new Vector3(sqrtOfThree / 2, 0 - i * thickness, 0.5f) * SCALE;
            verts[(i * 7) + 3] = new Vector3(sqrtOfThree / 2, 0 - i * thickness, -0.5f) * SCALE;
            verts[(i * 7) + 4] = new Vector3(0, 0 - i * thickness, -1) * SCALE;
            verts[(i * 7) + 5] = new Vector3(-sqrtOfThree / 2, 0 - i * thickness, -0.5f) * SCALE;
            verts[(i * 7) + 6] = new Vector3(-sqrtOfThree / 2, 0 - i * thickness, 0.5f) * SCALE;
        }
    }
    void drawHexagon() {
        int i;
 
        for (i = 0; i < 6; i++)
        {
            drawingPattern[i * 3 + 0] = 0;
            drawingPattern[i * 3 + 1] = i + 1;
            drawingPattern[i * 3 + 2] = i < 5 ? i + 2 :  1;
        }

        for (i = 0; i < 6; i++)
        {
            drawingPattern[18+ i * 3 + 0] = 7;
            drawingPattern[18+ i * 3 + 1] = 7 + i + 1;
            drawingPattern[18+ i * 3 + 2] = i > 0 ? 7 + i : 13;
        }

        for (i = 0; i < 6; i++)
        {
            drawingPattern[36 + i * 3] = i + 1;
            drawingPattern[37 + i * 3] = i + 8;
            drawingPattern[38 + i * 3] = i < 5 ? i + 2 : 1;
        }

        for (i = 0; i < 6; i++)
        {
            drawingPattern[54 + i * 3] = i > 0 ? i + 7 : 13;
            drawingPattern[55 + i * 3] = i + 8;
            drawingPattern[56 + i * 3] = i + 1;
        }
    }
}
