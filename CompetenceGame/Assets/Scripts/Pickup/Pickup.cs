﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {

    public TileSpawn tile;

    void OnTriggerEnter(Collider collider) {
        if (collider.gameObject == GameManager.instance.player.gameObject)
            tile.pickedUp();
    }

}
