﻿using UnityEngine;
using System.Collections;

public class ParticleSpawner : MonoBehaviour {

    public Particle particle;
    public float spawnInterval;
    public float startDelay;
    public AnimationCurve difficultyCurve;
    public float difficultyTimeScale;

    private float timestamp;
    private Particle obj;
    private Vector3 newPosition;

    void Start()
    {
        setTimestamp(startDelay);
    }

    void Update()
    {
        GameManager.instance.difficulty = difficultyCurve.Evaluate(Time.time / difficultyTimeScale);
        if (Time.time >= timestamp)
        {
            setTimestamp(spawnInterval - spawnInterval * GameManager.instance.difficulty * 0.33f);
            obj = Instantiate(particle);
            newPosition = new Vector3();

            if (coinToss())
            {
                newPosition.x = transform.position.x + transform.localScale.x * 5 * (coinToss() ? 1 : -1);
                newPosition.z = transform.position.z + transform.localScale.z * 5 * (Random.value * 2 - 1);
            }
            else
            {
                newPosition.x = transform.position.x + transform.localScale.x * 5 * (Random.value * 2 - 1);
                newPosition.z = transform.position.z + transform.localScale.z * 5 * (coinToss() ? 1 : -1);
            }

            obj.transform.position = newPosition;
        }
    }

    private bool coinToss()
    {
        return Random.value * 2 < 1;
    }

    private void setTimestamp(float interval)
    {
        timestamp = Time.time + interval;
    }
}
