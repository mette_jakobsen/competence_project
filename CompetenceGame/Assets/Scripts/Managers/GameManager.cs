﻿using UnityEngine;
using System.Collections;

public class GameManager{

    private static GameManager _instance;
    private int _resources = 0;
    private TileSpawner _spawner;
    private PlayerControls _player;
    private CameraMovement _camera;
    private Transform _finish;
    public TileSpawn playerTile;
    public float _difficulty;

    public GameManager()
    {
    }

    public float difficulty
    {
        get
        {
            return _difficulty;
        }
        set
        {
            _difficulty = value > 1 ? 1 : value;
        }
    }

    public Transform finish
    {
        get
        {
            if (_finish == null)
                _finish = GameObject.FindGameObjectWithTag("Finish").transform;
            return _finish;
        }
    }

    public CameraMovement camera
    {
        get
        {
            if (_camera == null)
                _camera = Object.FindObjectOfType<CameraMovement>() as CameraMovement;
            return _camera;
        }
    }

    public TileSpawner spawner
    {
        get
        {
            if (_spawner == null)
                _spawner = Object.FindObjectOfType<TileSpawner>() as TileSpawner;
            return _spawner;
        }
    }

    public PlayerControls player
    {
        get
        {
            if (_player == null)
                _player = GameObject.FindObjectOfType<PlayerControls>() as PlayerControls;
            return _player;
        }
    }

    public static GameManager instance
    {
        get
        {
            if (_instance == null)
                _instance = new GameManager();
            return _instance;
        }
    }
    public int resource
    {
        get
        {
            return _resources;
        }
    }

    public void removeResource()
    {
        if (--_resources == 0)
        {
            GameObject[] objects = GameObject.FindGameObjectsWithTag("Border");
            for (int i = 0; i < objects.Length; i++)
                objects[i].GetComponent<MeshRenderer>().enabled = false;
        }
    }

    public void addResource()
    {
        if (_resources++ == 0)
        {
            GameObject[] objects = GameObject.FindGameObjectsWithTag("Border");
            for (int i = 0; i < objects.Length; i++)
                objects[i].GetComponent<MeshRenderer>().enabled = true;
        }
    }

    public void GameLost()
    {
        _instance = null;
        Debug.Log("Game Lost");
        Application.LoadLevel("Lose");
    }

    public void GameWon()
    {
        _instance = null;
        Debug.Log("Game Won");
        Application.LoadLevel("Win");
    }
}
