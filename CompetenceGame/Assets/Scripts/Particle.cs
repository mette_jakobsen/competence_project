﻿using UnityEngine;
using System.Collections;

public class Particle : MonoBehaviour {

    public float force;

    private Vector3 direction, destination;
    private Rigidbody rgd;

    void Start()
    {
        rgd = GetComponent<Rigidbody>();
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Player");
        destination = objects[Random.Range(0, objects.Length - 1)].transform.position;
    }

    void FixedUpdate()
    {
        direction = (destination - transform.position).normalized;
        rgd.AddForce(direction * force * Time.deltaTime);
    }
}
